/*
 * Calculation Pi Approximation.
 */
 
double pi, h, sum, x;
int n, i;

void setup() {
  sum = 0.0;
  n = 100000000;
}

void loop() {
  h = 1.0 / (double) n;
  sum = 0.0;
  
  for(i = 1; i <= n; i++) {
    x = h * ((double)i - 0.5);
    sum += 4.0 / (1.0 + x*x);
  }
  
  pi = h * sum;
  
  Serial.println(pi, 27);
}
